#!/usr/bin/python
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from base64 import b64encode, decodestring

st1 = 'B'
st2 = 'vuSfftBXTtXzOGB7pC6WiJ3!Z9zy50LnvPU3ziBcGFShiyYaFdzv3iPTEvGS1!fN6ER61d1IZMIY1kg6-mIBMVFn!FZFm6LxbZ-X8C82SzYtPqo1T6MvRokUGuDZSg3B871bdLaAEEh4k6V9GAKTy5Tgo34Hyuo0fjVTIz6RJHSe-lCEs8LS61wqn!iNWhcI2!zxOSNCOtDp9rFvNqAy-w~~'

str2bin = lambda x: ' '.join(format(ord(y), 'b') for y in x)
str2b64 = lambda x: b64encode(x.encode('utf-8'))
str2dec = lambda x: ' '.join(format(ord(y), 'd') for y in x)
link2b64 = lambda x: x.replace('~', '=').replace('!', '/').replace('-', '+').encode('utf-8')
b64d = lambda x: decodestring(x)

a = lambda x: [format(ord(y), 'b') for y in x]
print(a(st2))