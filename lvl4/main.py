import requests
import re
import base64
import binascii

host = 'http://35.190.155.168:5001/31930212d6'
post = 'eJDUEP45iUTzYJu1w57t-10iT!PL!!iP0nI1s5ZFZ7kLWzbF!EXOtva8LEw2pqHREHVdkTFxGfn8chnuEuUFRUv5SB9u7H5WrTUW90c6AJ5q6bjM7wUxBlc35KvJ1FgKxCqyxxiFHfS3tgv8LwK1MlNQ7rtGMLWkZvSaZ!GzZDwvkhJKCDEMNE-efsMcD8OwzJbO!pEjunXa8idmBQxBpg~~'
url = host+'/?post='+post

testString = 'Vincent'

link2b64 = lambda x: x.replace('~', '=').replace('!', '/').replace('-', '+')
b642link = lambda x: x.replace('=', '~').replace('/', '!').replace('+', '-')
hex2bits = lambda x: "".join(["{:08b}".format(y) for y in x])

response = requests.get(host+'/?post='+b642link(link2b64(post)))
decodedPost = base64.b64decode(link2b64(post))
bitPost = hex2bits(decodedPost)
bitListPost = list(bitPost)

print(bitPost)
