import random
from tabulate import tabulate

#
# state = 3244526807126992633
debugSt = []
def setup(seed):
    global state
    state = 0
    for i in range(16):
        cur = seed & 3
        seed >>= 2
        state = (state << 4) | ((state & 3) ^ cur)
        state |= cur << 2


def next(bits):
    global state
    ret = 0
    for i in range(bits):
        ret <<= 1
        ret |= state & 1
        state = (state << 1) ^ (state >> 61)
        state &= 0xFFFFFFFFFFFFFFFF
        state ^= 0xFFFFFFFFFFFFFFFF

        for j in range(0, 64, 4):
            cur = (state >> j) & 0xF
            cur = (cur >> 3) | ((cur >> 2) & 2) | ((cur << 3) & 8) | ((cur << 2) & 4)
            state ^= cur << j
    return ret

def debug(line, state, ret, i):
    global debugSt
    if (i):
        debugSt.append([line, state, bin(state), ret, bin(ret)])

def space(s): return " ".join(s[i:i+4] for i in range(0, len(s), 4))
def tobin(s): return space(bin(s)[2:].zfill(64))

